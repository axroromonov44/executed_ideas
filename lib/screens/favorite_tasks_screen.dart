import 'package:executed_ideas/bloc/bloc_exports.dart';
import 'package:executed_ideas/models/task.dart';
import 'package:executed_ideas/widgets/tasks_list.dart';
import 'package:flutter/material.dart';

class FavoriteTasksScreen extends StatelessWidget {
  const FavoriteTasksScreen({Key? key}) : super(key: key);
  static const id = 'favorite_tasks_screen';

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TasksBloc, TasksState>(
      builder: (context, state) {
        List<TaskModel> taskList = state.favoriteTasks;
        return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Chip(
                backgroundColor: Colors.blueGrey,
                label: Text(
                  '${taskList.length} Tasks:',
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
            TasksList(
              taskList: taskList,
            ),
          ],
        );
      },
    );
  }
}
