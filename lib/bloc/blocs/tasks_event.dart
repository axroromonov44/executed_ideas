part of 'tasks_bloc.dart';

abstract class TasksEvent extends Equatable {
  const TasksEvent();
}

class AddTaskEvent extends TasksEvent {
  final TaskModel taskModel;

  const AddTaskEvent({
    required this.taskModel,
  });

  @override
  List<Object?> get props => [taskModel];
}

class UpdateTaskEvent extends TasksEvent {
  final TaskModel taskModel;

  const UpdateTaskEvent({
    required this.taskModel,
  });

  @override
  List<Object?> get props => [taskModel];
}

class RemoveTaskEvent extends TasksEvent {
  final TaskModel taskModel;

  const RemoveTaskEvent({
    required this.taskModel,
  });

  @override
  List<Object?> get props => [taskModel];
}

class DeleteTaskEvent extends TasksEvent {
  final TaskModel taskModel;

  const DeleteTaskEvent({
    required this.taskModel,
  });

  @override
  List<Object?> get props => [taskModel];
}

class MarkFavoriteOrUnFavoriteEvent extends TasksEvent {
  final TaskModel taskModel;

  const MarkFavoriteOrUnFavoriteEvent({required this.taskModel});

  @override
  List<Object?> get props => [taskModel];
}

class EditedTaskEvent extends TasksEvent {
  final TaskModel oldTaskModel;
  final TaskModel newTaskModel;

  const EditedTaskEvent({
    required this.oldTaskModel,
    required this.newTaskModel,
  });

  @override
  List<Object?> get props => [oldTaskModel, newTaskModel];
}
