import 'package:executed_ideas/screens/add_task_screen.dart';
import 'package:executed_ideas/screens/completed_tasks_screen.dart';
import 'package:executed_ideas/screens/favorite_tasks_screen.dart';
import 'package:executed_ideas/screens/my_drawer.dart';
import 'package:executed_ideas/screens/pending_tasks_screen.dart';
import 'package:flutter/material.dart';

class TabsScreen extends StatefulWidget {
  const TabsScreen({Key? key}) : super(key: key);
  static const id = 'tabs_screen';

  @override
  State<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  final List<Map<String, dynamic>> _pageDetails = [
    {'pageName': const PendingTasksScreen(), 'title': 'Executed Ideas'},
    {'pageName': const CompletedTasksScreen(), 'title': 'Completed Ideas'},
    {'pageName': const FavoriteTasksScreen(), 'title': 'Favorite Ideas'},
  ];

  var _selectedPageIndex = 0;

  void _addTask(BuildContext context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) => SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: const AddTaskScreen(),
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text(
          _pageDetails[_selectedPageIndex]['title'],
        ),
        actions: [
          IconButton(
            onPressed: () => _selectedPageIndex == 0 ? _addTask(context) : null,
            icon: Icon(_selectedPageIndex == 0 ? Icons.add : null),
          )
        ],
      ),
      drawer: const MyDrawer(),
      body: _pageDetails[_selectedPageIndex]['pageName'],
      floatingActionButton: _selectedPageIndex == 0
          ? FloatingActionButton(
              backgroundColor: Colors.blueGrey,
              onPressed: () => _addTask(context),
              tooltip: 'Add Task',
              child: const Icon(Icons.add),
            )
          : null,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedPageIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.black,
        backgroundColor: Colors.blueGrey,
        onTap: (index) {
          setState(() {
            _selectedPageIndex = index;
          });
        },
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.incomplete_circle_sharp),
            label: 'Pending Ideas',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.done),
            label: 'Completed Ideas',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favorite Ideas',
          ),
        ],
      ),
    );
  }
}
