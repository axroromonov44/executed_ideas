part of 'tasks_bloc.dart';

class TasksState extends Equatable {
  final List<TaskModel> pendingTasks;
  final List<TaskModel> completedTasks;
  final List<TaskModel> favoriteTasks;
  final List<TaskModel> removeTasks;

  const TasksState({
    this.pendingTasks = const <TaskModel>[],
    this.completedTasks = const <TaskModel>[],
    this.favoriteTasks = const <TaskModel>[],
    this.removeTasks = const <TaskModel>[],
  });

  @override
  List<Object?> get props => [
        pendingTasks,
        completedTasks,
        favoriteTasks,
        removeTasks,
      ];

  Map<String, dynamic> toMap() {
    return {
      'pendingTasks': pendingTasks.map((e) => e.toMap()).toList(),
      'completedTasks': completedTasks.map((e) => e.toMap()).toList(),
      'favoriteTasks': favoriteTasks.map((e) => e.toMap()).toList(),
      'removeTasks': removeTasks.map((e) => e.toMap()).toList(),
    };
  }

  factory TasksState.fromMap(Map<String, dynamic> map) {
    return TasksState(
      pendingTasks: List<TaskModel>.from(
          map['pendingTasks'].map((x) => TaskModel.fromMap(x))),
      completedTasks: List<TaskModel>.from(
          map['completedTasks'].map((x) => TaskModel.fromMap(x))),
      favoriteTasks: List<TaskModel>.from(
          map['favoriteTasks'].map((x) => TaskModel.fromMap(x))),
      removeTasks: List<TaskModel>.from(
          map['removeTasks'].map((x) => TaskModel.fromMap(x))),
    );
  }
}
