import 'package:executed_ideas/widgets/tasks_list.dart';
import 'package:flutter/material.dart';

import '../bloc/bloc_exports.dart';
import 'my_drawer.dart';

class RecycleBin extends StatelessWidget {
  const RecycleBin({Key? key}) : super(key: key);
  static const id = 'recycle_bin';

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TasksBloc, TasksState>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: Colors.white,
          drawer: const MyDrawer(),
          appBar: AppBar(
            backgroundColor: Colors.blueGrey,
            title: const Text(
              'Recycle Bin ',
            ),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Chip(
                  backgroundColor: Colors.blueGrey,
                  label: Text(
                    '${state.removeTasks.length} Tasks',
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ),
              TasksList(
                taskList: state.removeTasks,
              ),
            ],
          ),
        );
      },
    );
  }
}
