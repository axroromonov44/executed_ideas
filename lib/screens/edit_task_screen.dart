import 'package:executed_ideas/bloc/bloc_exports.dart';
import 'package:executed_ideas/models/task.dart';
import 'package:executed_ideas/services/guid_gen.dart';
import 'package:flutter/material.dart';

class EditTaskScreen extends StatelessWidget {
  final TaskModel oldTaskModel;

  const EditTaskScreen({
    Key? key,
    required this.oldTaskModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController titleController =
        TextEditingController(text: oldTaskModel.title);
    TextEditingController descriptionController =
        TextEditingController(text: oldTaskModel.description);
    return Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          const Text(
            'Edit task',
            style: TextStyle(
              fontSize: 24,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: TextField(
              autofocus: true,
              controller: titleController,
              decoration: const InputDecoration(
                label: Text('title'),
                border: OutlineInputBorder(),
              ),
            ),
          ),
          TextField(
            autofocus: true,
            controller: descriptionController,
            maxLines: 5,
            minLines: 3,
            decoration: const InputDecoration(
              label: Text('description'),
              border: OutlineInputBorder(),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: const Text('cancel'),
              ),
              ElevatedButton(
                onPressed: () {
                  var editedTask = TaskModel(
                    title: titleController.text,
                    description: descriptionController.text,
                    id: oldTaskModel.id,
                    isDone: false,
                    isFavorite: oldTaskModel.isFavorite,
                    date: DateTime.now().toString(),
                  );
                  context.read<TasksBloc>().add(EditedTaskEvent(
                        oldTaskModel: oldTaskModel,
                        newTaskModel: editedTask,
                      ));
                  Navigator.pop(context);
                },
                child: const Text('Save'),
              )
            ],
          )
        ],
      ),
    );
  }
}
