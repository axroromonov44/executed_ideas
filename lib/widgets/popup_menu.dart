import 'package:executed_ideas/models/task.dart';
import 'package:flutter/material.dart';

class PopupMenu extends StatelessWidget {
  final TaskModel taskModel;
  final VoidCallback cancelOrDeleteCallBack;
  final VoidCallback likeOrDislikeCallBack;
  final VoidCallback editTaskCallBack;

  const PopupMenu({
    Key? key,
    required this.cancelOrDeleteCallBack,
    required this.likeOrDislikeCallBack,
    required this.editTaskCallBack,
    required this.taskModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      itemBuilder: taskModel.isDeleted == false
          ? ((context) => [
                PopupMenuItem(
                  onTap: null,
                  child: TextButton.icon(
                    onPressed: editTaskCallBack,
                    icon: const Icon(Icons.edit,color: Colors.black,),
                    label: const Text(
                      'Edit',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
                PopupMenuItem(
                  onTap: likeOrDislikeCallBack,
                  child: TextButton.icon(
                      onPressed: null,
                      icon: Icon(taskModel.isFavorite == false
                          ? Icons.bookmark_add_outlined
                          : Icons.bookmark_remove,color: Colors.black,),
                      label: Text(
                          taskModel.isFavorite == false
                              ? 'Add to Bookmarks'
                              : 'Remove from Bookmarks',
                          style: const TextStyle(color: Colors.black))),
                ),
                PopupMenuItem(
                  onTap: cancelOrDeleteCallBack,
                  child: TextButton.icon(
                    onPressed: null,
                    icon: const Icon(Icons.edit,color: Colors.black,),
                    label: const Text('Delete',
                        style: TextStyle(color: Colors.black)),
                  ),
                ),
              ])
          : (context) => [
                PopupMenuItem(
                  onTap: () {},
                  child: TextButton.icon(
                    onPressed: null,
                    icon: const Icon(Icons.restore_from_trash),
                    label: const Text('Restore'),
                  ),
                ),
                PopupMenuItem(
                  onTap: cancelOrDeleteCallBack,
                  child: TextButton.icon(
                    onPressed: null,
                    icon: const Icon(Icons.delete_forever),
                    label: const Text(
                      'Delete Forever',
                    ),
                  ),
                ),
              ],
    );
  }
}
