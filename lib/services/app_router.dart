import 'package:executed_ideas/screens/recyle_bin.dart';
import 'package:executed_ideas/screens/tabs_screen.dart';
import 'package:executed_ideas/screens/pending_tasks_screen.dart';
import 'package:flutter/cupertino.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case RecycleBin.id:
        return CupertinoPageRoute(
          builder: (_) => const RecycleBin(),
        );
      case TabsScreen.id:
        return CupertinoPageRoute(
          builder: (_) => const TabsScreen(),
        );
      default:
        return null;
    }
  }
}
