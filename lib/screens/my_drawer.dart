import 'package:executed_ideas/screens/recyle_bin.dart';
import 'package:executed_ideas/screens/tabs_screen.dart';
import 'package:executed_ideas/screens/pending_tasks_screen.dart';
import 'package:flutter/material.dart';

import '../bloc/bloc_exports.dart';

class MyDrawer extends StatefulWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  State<MyDrawer> createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        backgroundColor: Colors.white,
        child: Column(
          children: [
            Container(
              color: Colors.blueGrey,
              width: double.infinity,
              padding: const EdgeInsets.symmetric(
                vertical: 14,
                horizontal: 20,
              ),
              child: const Text(
                'Assalamu alaikum ',
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
            BlocBuilder<TasksBloc, TasksState>(
              builder: (context, state) {
                return GestureDetector(
                  onTap: () => Navigator.of(context)
                      .pushReplacementNamed(TabsScreen.id),
                  child: ListTile(
                    leading: const Icon(
                      Icons.folder_special,
                      color: Colors.blueGrey,
                    ),
                    trailing: Text(
                      '${state.pendingTasks.length} | ${state.completedTasks.length}',
                      style: const TextStyle(color: Colors.black, fontSize: 16),
                    ),
                    title: const Text(
                      'My Ideas',
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                  ),
                );
              },
            ),
            const Divider(),
            BlocBuilder<TasksBloc, TasksState>(
              builder: (context, state) {
                return GestureDetector(
                  onTap: () =>
                      Navigator.of(context).pushReplacementNamed(RecycleBin.id),
                  child: ListTile(
                    leading: const Icon(Icons.delete, color: Colors.blueGrey),
                    trailing: Text(
                      '${state.removeTasks.length}',
                      style: const TextStyle(color: Colors.black, fontSize: 16),
                    ),
                    title: const Text(
                      'Bin',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
