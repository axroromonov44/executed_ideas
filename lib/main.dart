import 'package:executed_ideas/models/task.dart';
import 'package:executed_ideas/screens/tabs_screen.dart';
import 'package:executed_ideas/screens/pending_tasks_screen.dart';
import 'package:executed_ideas/services/app_router.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

import 'bloc/bloc_exports.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final storage = await HydratedStorage.build(
      storageDirectory: await getApplicationDocumentsDirectory());

  HydratedBlocOverrides.runZoned(
      () => runApp(MyApp(
            appRouter: AppRouter(),
          )),
      storage: storage);
}

class MyApp extends StatelessWidget {
  final AppRouter appRouter;

  const MyApp({
    Key? key,
    required this.appRouter,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TasksBloc(),
      child: MaterialApp(
        onGenerateRoute: appRouter.onGenerateRoute,
        home: const TabsScreen(),
      ),
    );
  }
}
