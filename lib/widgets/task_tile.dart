import 'dart:ffi';

import 'package:executed_ideas/models/task.dart';
import 'package:executed_ideas/screens/edit_task_screen.dart';
import 'package:executed_ideas/widgets/popup_menu.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../bloc/bloc_exports.dart';

class TaskTile extends StatelessWidget {
  final TaskModel task;

  const TaskTile({
    Key? key,
    required this.task,
  }) : super(key: key);

  void _removeOrDeleteTask(BuildContext context, TaskModel taskModel) {
    task.isDeleted!
        ? context.read<TasksBloc>().add(DeleteTaskEvent(taskModel: task))
        : context.read<TasksBloc>().add(RemoveTaskEvent(taskModel: task));
  }

  void _editTask(BuildContext context) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (context) => SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: EditTaskScreen(
                  oldTaskModel: task,
                ),
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 10,
      ),
      child: Row(
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(
                    task.isFavorite == false ? Icons.star_outline : Icons.star),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        task.title,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 18,
                          decoration:
                              task.isDone! ? TextDecoration.lineThrough : null,
                        ),
                      ),
                      Text(DateFormat()
                          .add_yMMMd()
                          .add_Hms()
                          .format(DateTime.parse(task.date)))
                    ],
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Checkbox(
                checkColor: Colors.white,
                fillColor: MaterialStateProperty.all(Colors.blueGrey),
                value: task.isDone,
                onChanged: task.isDeleted == false
                    ? (value) {
                        context
                            .read<TasksBloc>()
                            .add(UpdateTaskEvent(taskModel: task));
                      }
                    : null,
              ),
              PopupMenu(
                taskModel: task,
                editTaskCallBack: () {
                  _editTask(context);
                 },
                cancelOrDeleteCallBack: () =>
                    _removeOrDeleteTask(context, task),
                likeOrDislikeCallBack: () => context.read<TasksBloc>().add(
                      MarkFavoriteOrUnFavoriteEvent(
                        taskModel: task,
                      ),
                    ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

// ListTile(
// title: Text(
// task.title,
// overflow: TextOverflow.ellipsis,
// style: TextStyle(
// decoration: task.isDone! ? TextDecoration.lineThrough : null,
// ),
// ),
// trailing: Checkbox(
// checkColor: Colors.white,
// fillColor: MaterialStateProperty.all(Colors.blueGrey),
// value: task.isDone,
// onChanged: task.isDeleted == false
// ? (value) {
// context.read<TasksBloc>().add(
// UpdateTaskEvent(taskModel: task),
// );
// }
// : null,
// ),
// onLongPress: () => _removeOrDeleteTask(context, task),
// )
