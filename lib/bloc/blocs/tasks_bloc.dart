import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:executed_ideas/bloc/bloc_exports.dart';
import 'package:executed_ideas/models/task.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'tasks_event.dart';

part 'tasks_state.dart';

class TasksBloc extends HydratedBloc<TasksEvent, TasksState> {
  TasksBloc() : super(const TasksState()) {
    on<AddTaskEvent>(_onAddTask);
    on<UpdateTaskEvent>(_onUpdateTask);
    on<DeleteTaskEvent>(_onDeleteTask);
    on<RemoveTaskEvent>(_onRemoveTask);
    on<MarkFavoriteOrUnFavoriteEvent>(_onMarkFavoriteOrUnFavorite);
    on<EditedTaskEvent>(_onEditedTask);
  }

  void _onAddTask(AddTaskEvent event, Emitter<TasksState> emit) {
    final state = this.state;
    emit(TasksState(
      pendingTasks: List.from(state.pendingTasks)..add(event.taskModel),
      completedTasks: state.completedTasks,
      favoriteTasks: state.favoriteTasks,
      removeTasks: state.removeTasks,
    ));
  }

  void _onUpdateTask(UpdateTaskEvent event, Emitter<TasksState> emit) {
    final state = this.state;
    final task = event.taskModel;
    List<TaskModel> pendingTasks = state.pendingTasks;
    List<TaskModel> completedTasks = state.completedTasks;
    task.isDone == false
        ? {
            pendingTasks = List.from(pendingTasks)..remove(task),
            completedTasks = List.from(completedTasks)
              ..insert(0, task.copyWith(isDone: true))
          }
        : {
            completedTasks = List.from(completedTasks)..remove(task),
            pendingTasks = List.from(pendingTasks)
              ..insert(0, task.copyWith(isDone: false))
          };

    emit(TasksState(
        pendingTasks: pendingTasks,
        completedTasks: completedTasks,
        favoriteTasks: state.favoriteTasks,
        removeTasks: state.removeTasks));
  }

  void _onDeleteTask(DeleteTaskEvent event, Emitter<TasksState> emit) {
    final state = this.state;

    emit(TasksState(
      pendingTasks: state.pendingTasks,
      completedTasks: state.completedTasks,
      favoriteTasks: state.favoriteTasks,
      removeTasks: List.from(state.removeTasks)..remove(event.taskModel),
    ));
  }

  void _onRemoveTask(RemoveTaskEvent event, Emitter<TasksState> emit) {
    final state = this.state;

    emit(
      TasksState(
          pendingTasks: List.from(state.pendingTasks)..remove(event.taskModel),
          completedTasks: List.from(state.completedTasks)
            ..remove(event.taskModel),
          favoriteTasks: List.from(state.favoriteTasks)
            ..remove(event.taskModel),
          removeTasks: List.from(state.removeTasks)
            ..add(event.taskModel.copyWith(isDeleted: true))),
    );
  }

  void _onMarkFavoriteOrUnFavorite(
      MarkFavoriteOrUnFavoriteEvent event, Emitter<TasksState> emit) {
    final state = this.state;
    List<TaskModel> pendingTasks = state.pendingTasks;
    List<TaskModel> completedTasks = state.completedTasks;
    List<TaskModel> favoriteTasks = state.favoriteTasks;
    if (event.taskModel.isDone == false) {
      if (event.taskModel.isFavorite == false) {
        var taskIndex = pendingTasks.indexOf(event.taskModel);
        pendingTasks = List.from(pendingTasks)
          ..remove(event.taskModel)
          ..insert(taskIndex, event.taskModel.copyWith(isFavorite: true));
        favoriteTasks.insert(0, event.taskModel.copyWith(isFavorite: true));
      } else {
        var taskIndex = pendingTasks.indexOf(event.taskModel);
        pendingTasks = List.from(pendingTasks)
          ..remove(event.taskModel)
          ..insert(taskIndex, event.taskModel.copyWith(isFavorite: false));
        favoriteTasks.remove(event.taskModel);
      }
    } else {
      if (event.taskModel.isFavorite == false) {
        var taskIndex = completedTasks.indexOf(event.taskModel);
        completedTasks = List.from(completedTasks)
          ..remove(event.taskModel)
          ..insert(taskIndex, event.taskModel.copyWith(isFavorite: true));
        favoriteTasks.insert(0, event.taskModel.copyWith(isFavorite: true));
      } else {
        var taskIndex = completedTasks.indexOf(event.taskModel);
        completedTasks = List.from(completedTasks)
          ..remove(event.taskModel)
          ..insert(taskIndex, event.taskModel.copyWith(isFavorite: false));
        favoriteTasks.remove(event.taskModel);
      }
    }
    emit(TasksState(
      pendingTasks: pendingTasks,
      completedTasks: completedTasks,
      favoriteTasks: favoriteTasks,
      removeTasks: state.removeTasks,
    ));
  }

  void _onEditedTask(EditedTaskEvent event, Emitter<TasksState> emit) {
    final state = this.state;
    List<TaskModel> favoriteTasks = state.favoriteTasks;
    if (event.oldTaskModel.isFavorite == true) {
      favoriteTasks
        ..remove(event.oldTaskModel)
        ..insert(0, event.newTaskModel);
    }
    emit(
      TasksState(
        pendingTasks: List.from(state.pendingTasks)
          ..remove(event.oldTaskModel)
          ..insert(0, event.newTaskModel),
        completedTasks: state.completedTasks..remove(event.oldTaskModel),
        favoriteTasks: favoriteTasks,
        removeTasks: state.removeTasks,
      ),
    );
  }

  @override
  TasksState? fromJson(Map<String, dynamic> json) {
    return TasksState.fromMap(json);
  }

  @override
  Map<String, dynamic>? toJson(TasksState state) {
    return state.toMap();
  }
}
