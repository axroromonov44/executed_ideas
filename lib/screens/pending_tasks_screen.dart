import 'package:executed_ideas/models/task.dart';
import 'package:executed_ideas/screens/add_task_screen.dart';
import 'package:executed_ideas/screens/my_drawer.dart';
import 'package:executed_ideas/widgets/tasks_list.dart';
import 'package:flutter/material.dart';

import '../bloc/bloc_exports.dart';

class PendingTasksScreen extends StatelessWidget {
  const PendingTasksScreen({Key? key}) : super(key: key);
  static const id = 'pending_tasks_screen';

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TasksBloc, TasksState>(
      builder: (context, state) {
        List<TaskModel> taskList = state.pendingTasks;
        return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Chip(
                backgroundColor: Colors.blueGrey,
                label: Text(
                  '${taskList.length} Pending | ${state.completedTasks.length} Completed',
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
            TasksList(
              taskList: taskList,
            ),
          ],
        );
      },
    );
  }
}
